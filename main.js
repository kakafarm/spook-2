async function init() {
  const [processInput, callCountRef] = await Scheme.load_main("/main.wasm", {});

  document.getElementById("submit").onclick = () => {
    const input = document.getElementById("input").value;
    const output = processInput.call(input);
    document.getElementById("output").innerText = output;
    document.getElementById("call-count").innerText = BigInt(
      callCountRef.call()
    );

    return false;
  };
}

window.addEventListener("load", init);
