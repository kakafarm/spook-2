(let ((call-count 0))
  (define (process-input input)
    (set! call-count (+ call-count 1))
    (string-upcase input))

  (define (call-count-ref)
    call-count)

  ;; Use `values` to export functions to JavaScript
  (values process-input call-count-ref))
