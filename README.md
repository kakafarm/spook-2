# guile-hoot-starter

A starter project for building Guile Hoot apps that auto-publish to a Codeberg Pages site.

## Getting Started

Create a new Codeberg repository using this repository as a template by [clicking here](https://codeberg.org/repo/create?template_id=153883)!

Once your repository is created, clone it locally and use Guix to build and test the code locally:

```sh
# Create a Guix shell with dependencies installed:
guix shell -m manifest.scm

# Inside the Guix shell:
./scripts/build
./scripts/serve
```

## Setting Up Automatic Publishing

To automatically publish your changes to a Codeberg Pages site, you'll need to follow these steps.  First, create a token you can use to automate the publishing of your website:

1. Open your [user settings page](https://codeberg.org/user/settings) on Codeberg
2. Click the "Applications" tab on the left side of the page
3. Under the "Generate New Token" section, create a token named "Website Push Token", expand "Select permissions", then change the "repository" setting to "Read and Write", then click "Generate Token"
4. Copy the token that is shown at the top of the page and store it somewhere temporarily

Now that you have your token, set up CI for your Codeberg repo:

1. Log into https://ci.codeberg.org with your Codeberg account
2. Click "Add repository", search for your repo name, then click "Enable"
3. Click the "Settings" wheel icon at the top of the CI page for your repo
4. Click the "Secrets" tab at the top
5. Click "Add Secret" and create a secret named "repo_token" with the token you created as the value
6. Click the "Add secret" button at the bottom to save the token

Finally, edit the `.woodpecker.yml` file in your repo to make the following changes:

1. Change the `user.name` and `user.email` settings used in the `git config` calls:

   ```sh
   git config --global user.email "david@daviwil.com"
   git config --global user.name "David Wilson"
   ```

2. Change the `git remote add` invocation to use your Codeberg username and correct repo URL:

   ```sh
   git remote add upstream https://daviwil:$REPO_TOKEN@codeberg.org/daviwil/guile-hoot-starter.git
   ```
